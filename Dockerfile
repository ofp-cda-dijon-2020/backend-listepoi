# Build the package
FROM openjdk:16-jdk-alpine AS build
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN ./gradlew --no-daemon assemble

# Runtime
FROM openjdk:16-jdk-alpine
RUN mkdir /project
WORKDIR /project
COPY --from=build /project/build/libs/listpoi-0.0.1-SNAPSHOT.jar listpoi-0.0.1-SNAPSHOT.jar
CMD java -jar listpoi-0.0.1-SNAPSHOT.jar

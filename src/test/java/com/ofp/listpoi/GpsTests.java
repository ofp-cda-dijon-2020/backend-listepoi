package com.ofp.listpoi;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GpsTests {
    @Test
    public void testDistance() {
        assertEquals(0, GpsHelpers.computedDistance(0, 0, 0, 0));
        assertEquals(5918.185064088764, GpsHelpers.computedDistance(51.5, 0, 38.8, -77.1));
    }

}
package com.ofp.listpoi.jpa;

import com.ofp.listpoi.entities.Feature;
import com.ofp.listpoi.repositories.FeatureRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class FeatureTests {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private FeatureRepository featureRepository;


    @Test
    public void canCreateFeature() {
        Feature feature = new Feature("icon", "desc");
        featureRepository.save(feature);

        Iterable<Feature> categories = featureRepository.findAll();
        assertEquals(1, IterableUtil.sizeOf(categories));
        assertEquals("icon", categories.iterator().next().getIconName());
        assertEquals("desc", categories.iterator().next().getDescription());
    }

    @Test
    public void canGetById() {
        Feature feature = entityManager.persistAndFlush(new Feature("icon", "desc"));

        Optional<Feature> found = featureRepository.findById(feature.getId());
        assertTrue(found.isPresent());
        assertEquals(feature.getId(), found.get().getId());
    }

    @Test
    public void canUpdate() {
        Feature feature = entityManager.persistAndFlush(new Feature("canUpdateIcon", "canUpdateDesc"));

        feature.setIconName("updatedIcon");
        feature.setDescription("updatedDescription");
        featureRepository.save(feature);

        Feature found = featureRepository.findById(feature.getId()).orElseThrow();
        assertEquals("updatedIcon", found.getIconName());
        assertEquals("updatedDescription", found.getDescription());
    }

    @Test
    public void canDeleteById() {
        Feature feature = entityManager.persistAndFlush(new Feature("canDeleteIcon", "canDeleteDesc"));

        featureRepository.deleteById(feature.getId());
        assertTrue(featureRepository.findById(feature.getId()).isEmpty());
    }

    @Test
    public void canList() {
        entityManager.persistAndFlush(new Feature("icon1", "desc1"));
        entityManager.persistAndFlush(new Feature("icon2", "desc2"));
        entityManager.persistAndFlush(new Feature("icon3", "desc3"));

        ArrayList<Feature> list = new ArrayList<>(IterableUtil.toCollection(featureRepository.findAll()));
        assertEquals(3, list.size());
        assertEquals("icon1", list.get(0).getIconName());
        assertEquals("icon2", list.get(1).getIconName());
        assertEquals("icon3", list.get(2).getIconName());
    }

}

package com.ofp.listpoi.jpa;

import com.ofp.listpoi.GpsHelpers;
import com.ofp.listpoi.entities.*;
import com.ofp.listpoi.repositories.POIRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class POITests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private POIRepository poiRepository;

    // Default test instances
    private POI testPOI;
    private Category category1;
    private Category category2;
    private User user1;
    private User user2;
    private Tag tag1;
    private Tag tag2;
    private Feature feature1;
    private Feature feature2;

    // Helper function to initialize some POIs in table
    private void persistLists() {

        entityManager.persistAndFlush(new POI(
                45.75918157292687,
                4.5983696861981365,
                "name1",
                "description1",
                0,
                null,
                new Date(),
                user1,
                category1,
                List.of(tag1),
                List.of(feature2)));


        entityManager.persistAndFlush(new POI(
                45.76366608555091,
                5.048333043509066,
                "name2",
                "description2",
                0,
                null,
                new Date(),
                user2,
                category1,
                List.of(tag2),
                List.of(feature2)));


        entityManager.persistAndFlush(new POI(
                45.30800563523379,
                4.873490138953962,
                "name3",
                "description3",
                0,
                null,
                new Date(),
                user1,
                category2,
                List.of(tag1),
                List.of(feature1)));
    }

    @BeforeEach
    private void setupEntities() {
        tag1 = entityManager.persist(new Tag("tag1"));
        tag2 = entityManager.persist(new Tag("tag2"));
        feature1 = entityManager.persist(new Feature("feature1", "description1"));
        feature2 = entityManager.persist(new Feature("feature2", "description2"));
        category1 = entityManager.persist(new Category("category1", "icon1"));
        category2 = entityManager.persist(new Category("category2", "icon2"));
        user1 = entityManager.persist(new User(true, true, true, "firstName1", "lastName1", "email1@test.com", "password"));
        user2 = entityManager.persist(new User(true, true, true, "firstName2", "lastName2", "email2@test.com", "password"));

        testPOI = new POI(0, 0, "name", "description", 0, null, new Date(), user1, category1);
    }

    @Test
    public void canCreatePOI() {
        poiRepository.save(testPOI);

        Iterable<POI> categories = poiRepository.findAll();
        assertEquals(1, IterableUtil.sizeOf(categories));

        POI found = categories.iterator().next();
        assertEquals("name", found.getName());
        assertEquals("description", found.getDescription());
    }

    @Test
    public void canGetById() {
        POI poi = entityManager.persistAndFlush(testPOI);

        Optional<POI> found = poiRepository.findById(poi.getId());
        assertTrue(found.isPresent());
        assertEquals(poi.getId(), found.get().getId());
    }

    @Test
    public void canUpdate() {
        POI poi = entityManager.persistAndFlush(testPOI);

        poi.setName("updatedName");
        poi.setDescription("updatedDescription");
        poiRepository.save(poi);

        POI found = poiRepository.findById(poi.getId()).orElseThrow();
        assertEquals("updatedName", found.getName());
        assertEquals("updatedDescription", found.getDescription());
    }

    @Test
    public void canDeleteById() {
        POI poi = entityManager.persistAndFlush(testPOI);

        poiRepository.deleteById(poi.getId());
        assertTrue(poiRepository.findById(poi.getId()).isEmpty());
    }

    @Test
    public void canList() {
        persistLists();

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAll()));
        assertEquals(3, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name2", list.get(1).getName());
        assertEquals("name3", list.get(2).getName());
    }

    @Test
    public void canFilterByName() {
        persistLists();

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.filterBy("me2")));
        assertEquals(1, list.size());
        assertEquals("name2", list.get(0).getName());
    }

    @Test
    public void canFilterByCreator() {
        persistLists();

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByCreatorId(user1.getId())));
        assertEquals(2, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name3", list.get(1).getName());

        list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByCreatorId(user2.getId())));
        assertEquals(1, list.size());
        assertEquals("name2", list.get(0).getName());
    }

    @Test
    public void canFilterByCategory() {
        persistLists();

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByCategoryId(category1.getId())));
        assertEquals(2, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name2", list.get(1).getName());

        list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByCategoryId(category2.getId())));
        assertEquals(1, list.size());
        assertEquals("name3", list.get(0).getName());
    }

    @Test
    public void canFilterByTag() {
        persistLists();

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByTagId(tag1.getId())));
        assertEquals(2, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name3", list.get(1).getName());

        list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByTagId(tag2.getId())));
        assertEquals(1, list.size());
        assertEquals("name2", list.get(0).getName());
    }

    @Test
    public void canFilterByFeature() {
        persistLists();

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByFeatureId(feature1.getId())));
        assertEquals(1, list.size());
        assertEquals("name3", list.get(0).getName());

        list = new ArrayList<>(IterableUtil.toCollection(poiRepository.findAllByFeatureId(feature2.getId())));
        assertEquals(2, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name2", list.get(1).getName());
    }

    @Test
    public void canFilterByDistanceSquare() {
        persistLists();

        entityManager.persistAndFlush(new POI(
                45.613546735905665,
                5.034621590475096,
                "name4",
                "description4",
                0,
                null,
                new Date(),
                user2,
                category1));

        GpsHelpers.BoundingBox box = GpsHelpers.computeBoundingBox(45.75738766694947, 4.854205995069208, 20);
        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.filterByDistanceSquare(box.getLatMin(), box.getLatMax(), box.getLonMin(), box.getLonMax())));
        assertEquals(3, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name2", list.get(1).getName());
        assertEquals("name4", list.get(2).getName());
    }

    @Test
    public void canFilterByDistance() {
        persistLists();

        entityManager.persistAndFlush(new POI(
                45.613546735905665,
                5.034621590475096,
                "name4",
                "description4",
                0,
                null,
                new Date(),
                user2,
                category1));

        ArrayList<POI> list = new ArrayList<>(IterableUtil.toCollection(poiRepository.filterByDistance(45.75738766694947, 4.854205995069208, 20)));
        assertEquals(2, list.size());
        assertEquals("name1", list.get(0).getName());
        assertEquals("name2", list.get(1).getName());
    }
}

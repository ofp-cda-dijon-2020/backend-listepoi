package com.ofp.listpoi.jpa;

import com.ofp.listpoi.entities.User;
import com.ofp.listpoi.repositories.UserRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class UserTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    // Default test instance
    private final User testUser = new User(true, true, true, "firstName", "lastName", "mail@test.com", "password");

    // Helper function to initialize some users in table
    private void persistLists() {
        entityManager.persistAndFlush(new User(true, true, true, "firstName1", "lastName1", "mail1@test.com", "password"));
        entityManager.persistAndFlush(new User(true, true, true, "firstName2", "lastName2", "mail2@test.com", "password"));
        entityManager.persistAndFlush(new User(true, true, true, "firstName3", "lastName3", "mail3@test.com", "password"));
    }

    @Test
    public void canCreateUser() {
        userRepository.save(testUser);

        Iterable<User> users = userRepository.findAll();
        assertEquals(1, IterableUtil.sizeOf(users));
        assertEquals("firstName", users.iterator().next().getFirstName());
        assertEquals("lastName", users.iterator().next().getLastName());
    }

    @Test
    public void canGetById() {
        User user = entityManager.persistAndFlush(testUser);

        Optional<User> found = userRepository.findById(user.getId());
        assertTrue(found.isPresent());
        assertEquals(user.getId(), found.get().getId());
    }

    @Test
    public void canGetByEmail() {
        User user = entityManager.persistAndFlush(testUser);

        Optional<User> found = userRepository.findByEmail(user.getEmail());
        assertTrue(found.isPresent());
        assertEquals(user.getId(), found.get().getId());
    }

    @Test
    public void cannotCreateUserWithExistingEmail() {
        entityManager.persistAndFlush(testUser);
        assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(new User(true, true, true, "firstName1", "lastName1", "mail@test.com", "password")));
    }

    @Test
    public void canUpdate() {
        User user = entityManager.persistAndFlush(testUser);

        user.setFirstName("newFirstName");
        user.setLastName("newLastName");
        userRepository.save(user);

        User found = userRepository.findById(user.getId()).orElseThrow();
        assertEquals("newFirstName", found.getFirstName());
        assertEquals("newLastName", found.getLastName());
    }

    @Test
    public void canDeleteById() {
        User user = entityManager.persistAndFlush(testUser);

        userRepository.deleteById(user.getId());
        assertTrue(userRepository.findById(user.getId()).isEmpty());
    }

    @Test
    public void canList() {
        persistLists();

        ArrayList<User> list = new ArrayList<>(IterableUtil.toCollection(userRepository.findAll()));
        assertEquals(3, list.size());
        assertEquals("firstName1", list.get(0).getFirstName());
        assertEquals("firstName2", list.get(1).getFirstName());
        assertEquals("firstName3", list.get(2).getFirstName());
    }

    @Test
    public void canFilterByName() {
        persistLists();

        ArrayList<User> list = new ArrayList<>(IterableUtil.toCollection(userRepository.filterBy("irst")));
        assertEquals(3, list.size());
        assertEquals("firstName1", list.get(0).getFirstName());
        assertEquals("firstName2", list.get(1).getFirstName());
        assertEquals("firstName3", list.get(2).getFirstName());
    }
}

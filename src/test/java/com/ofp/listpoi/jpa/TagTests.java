package com.ofp.listpoi.jpa;

import com.ofp.listpoi.entities.Tag;
import com.ofp.listpoi.repositories.TagRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class TagTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TagRepository tagRepository;

    // Default test instance
    private final Tag testTag = new Tag("name");

    // Helper function to initialize some tags in table
    private void persistLists() {
        entityManager.persistAndFlush(new Tag("first tag"));
        entityManager.persistAndFlush(new Tag("second tag"));
        entityManager.persistAndFlush(new Tag("third tag"));
    }

    @Test
    public void canCreateTag() {
        tagRepository.save(testTag);

        Iterable<Tag> tags = tagRepository.findAll();
        assertEquals(1, IterableUtil.sizeOf(tags));
        assertEquals("name", tags.iterator().next().getName());
    }

    @Test
    public void canGetById() {
        Tag tag = entityManager.persistAndFlush(testTag);

        Optional<Tag> found = tagRepository.findById(tag.getId());
        assertTrue(found.isPresent());
        assertEquals(tag.getId(), found.get().getId());
    }

    @Test
    public void cannotCreateTagWithExistingName() {
        entityManager.persistAndFlush(testTag);
        assertThrows(DataIntegrityViolationException.class, () -> tagRepository.save(new Tag("name")));
    }

    @Test
    public void canUpdate() {
        Tag tag = entityManager.persistAndFlush(testTag);

        tag.setName("updatedName");
        tagRepository.save(tag);

        Tag found = tagRepository.findById(tag.getId()).orElseThrow();
        assertEquals("updatedName", found.getName());
    }

    @Test
    public void canDeleteById() {
        Tag tag = entityManager.persistAndFlush(testTag);

        tagRepository.deleteById(tag.getId());
        assertTrue(tagRepository.findById(tag.getId()).isEmpty());
    }

    @Test
    public void canList() {
        persistLists();

        ArrayList<Tag> list = new ArrayList<>(IterableUtil.toCollection(tagRepository.findAll()));
        assertEquals(3, list.size());
        assertEquals("first tag", list.get(0).getName());
        assertEquals("second tag", list.get(1).getName());
        assertEquals("third tag", list.get(2).getName());
    }

    @Test
    public void canFilterByName() {
        persistLists();

        ArrayList<Tag> list = new ArrayList<>(IterableUtil.toCollection(tagRepository.findAllByNameContaining("ta")));
        assertEquals(3, list.size());
        assertEquals("first tag", list.get(0).getName());
        assertEquals("second tag", list.get(1).getName());
        assertEquals("third tag", list.get(2).getName());

        list = new ArrayList<>(IterableUtil.toCollection(tagRepository.findAllByNameContaining("ir")));
        assertEquals(2, list.size());
        assertEquals("first tag", list.get(0).getName());
        assertEquals("third tag", list.get(1).getName());
    }
}

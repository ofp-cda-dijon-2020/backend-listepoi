package com.ofp.listpoi.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ofp.listpoi.controllers.TagController;
import com.ofp.listpoi.entities.Tag;
import com.ofp.listpoi.repositories.TagRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@Import(RestTestsConfiguration.class)
@WebMvcTest(TagController.class)
public class TagTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TagRepository tagRepository;

    // Default test instance
    private final Tag testTag = new Tag("tag");

    // Default list instance
    private final Collection<Tag> testTags = Arrays.asList(
            new Tag("name1"),
            new Tag("name2"),
            new Tag("name3")
    );


    @Test
    @WithAnonymousUser
    public void cannotCreateWhenNotAuthenticated() throws Exception {
        mvc.perform(post("/api/v1/tags")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testTag)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canCreateTag() throws Exception {
        // Setup mock repository to return `testTag` when `save()` is called
        given(tagRepository.save(Mockito.any(Tag.class))).willReturn(testTag);

        mvc.perform(post("/api/v1/tags")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testTag)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.name", is(testTag.getName())));
    }


    @Test
    public void cannotGetNonExistingTag() throws Exception {
        mvc.perform(get("/api/v1/tags/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotGetWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/tags/1")).andExpect(status().isForbidden());
    }


    @Test
    public void canGetTagById() throws Exception {
        // Setup mock repository to return `testTag` when `save()` is called
        given(tagRepository.findById(Mockito.anyInt())).willReturn(Optional.of(testTag));

        mvc.perform(get("/api/v1/tags/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.name", is(testTag.getName())));

        verify(tagRepository).findById(2);
    }


    @Test
    public void cannotUpdateNonExistingTag() throws Exception {
        // Setup mock repository to return `false` when `existsById()` is called
        given(tagRepository.existsById(Mockito.anyInt())).willReturn(false);

        mvc.perform(put("/api/v1/tags/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testTag)))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotUpdateWhenNotAuthenticated() throws Exception {
        mvc.perform(put("/api/v1/tags/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testTag)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canUpdateTag() throws Exception {
        Tag updatedTag = new Tag("newName");

        // Setup mock repository to return `true` when `existsById()` is called
        given(tagRepository.existsById(Mockito.anyInt())).willReturn(true);
        // Setup mock repository to return `updatedTag` when `save()` is called
        given(tagRepository.save(Mockito.any(Tag.class))).willReturn(updatedTag);

        mvc.perform(put("/api/v1/tags/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(updatedTag)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.name", is("newName")));

        verify(tagRepository).existsById(3);
    }


    @Test
    public void cannotDeleteNonExistingTag() throws Exception {
        // Setup mock repository to return `false` when `existsById()` is called
        given(tagRepository.existsById(Mockito.anyInt())).willReturn(false);

        mvc.perform(delete("/api/v1/tags/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotDeleteWhenNotAuthenticated() throws Exception {
        mvc.perform(delete("/api/v1/tags/1")) .andExpect(status().isForbidden());
    }


    @Test
    public void canDeleteTag() throws Exception {
        // Setup mock repository to return `true` when `existsById()` is called
        given(tagRepository.existsById(Mockito.anyInt())).willReturn(true);

        mvc.perform(delete("/api/v1/tags/4"))
                .andExpect(status().isOk());

        verify(tagRepository).existsById(4);
        verify(tagRepository).deleteById(4);
    }


    @Test
    @WithAnonymousUser
    public void cannotListWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/tags")).andExpect(status().isForbidden());
    }


    @Test
    public void canListTags() throws Exception {
        // Setup mock repository to return `testTags` when `findAll()` is called
        given(tagRepository.findAll()).willReturn(testTags);

        mvc.perform(get("/api/v1/tags"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", is("name1")))
                .andExpect(jsonPath("$[1].name", is("name2")))
                .andExpect(jsonPath("$[2].name", is("name3")));

        verify(tagRepository).findAll();
    }


    @Test
    public void canFilterTags() throws Exception {
        // Setup mock repository to return `testTags` when `findByNameContaining()` is called
        given(tagRepository.findAllByNameContaining(Mockito.anyString())).willReturn(testTags);

        mvc.perform(get("/api/v1/tags")
                        .param("filter", "myFilter"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", is("name1")))
                .andExpect(jsonPath("$[1].name", is("name2")))
                .andExpect(jsonPath("$[2].name", is("name3")));

        verify(tagRepository).findAllByNameContaining("myFilter");
    }

}

package com.ofp.listpoi.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ofp.listpoi.controllers.FeatureController;
import com.ofp.listpoi.entities.Feature;
import com.ofp.listpoi.repositories.FeatureRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@Import(RestTestsConfiguration.class)
@WebMvcTest(FeatureController.class)
public class FeatureTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FeatureRepository featureRepository;

    // Default test instance
    private final Feature testFeature = new Feature("icon", "feature");

    // Default list instance
    private final Collection<Feature> testFeatures = Arrays.asList(
            new Feature("icon1", "feature1"),
            new Feature("icon2", "feature2"),
            new Feature("icon3", "feature3")
    );


    @Test
    @WithAnonymousUser
    public void cannotCreateWhenNotAuthenticated() throws Exception {
        mvc.perform(post("/api/v1/features")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testFeature)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canCreateFeature() throws Exception {
        // Setup mock repository to return `testFeature` when `save()` is called
        given(featureRepository.save(Mockito.any(Feature.class))).willReturn(testFeature);

        mvc.perform(post("/api/v1/features")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(testFeature)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.description", is(testFeature.getDescription())))
                .andExpect(jsonPath("$.iconName", is(testFeature.getIconName())));
    }


    @Test
    public void cannotGetNonExistingFeature() throws Exception {
        mvc.perform(get("/api/v1/features/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotGetWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/features/1")).andExpect(status().isForbidden());
    }


    @Test
    public void canGetFeatureById() throws Exception {
        // Setup mock repository to return `testFeature` when `save()` is called
        given(featureRepository.findById(Mockito.anyInt())).willReturn(Optional.of(testFeature));

        mvc.perform(get("/api/v1/features/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.description", is(testFeature.getDescription())))
                .andExpect(jsonPath("$.iconName", is(testFeature.getIconName())));

        verify(featureRepository).findById(2);
    }


    @Test
    public void cannotUpdateNonExistingFeature() throws Exception {
        // Setup mock repository to return `false` when `existsById()` is called
        given(featureRepository.existsById(Mockito.anyInt())).willReturn(false);

        mvc.perform(put("/api/v1/features/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(testFeature)))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotUpdateWhenNotAuthenticated() throws Exception {
        mvc.perform(put("/api/v1/features/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testFeature)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canUpdateFeature() throws Exception {
        Feature updatedFeature = new Feature("newIconName", "newDescription");

        // Setup mock repository to return `true` when `existsById()` is called
        given(featureRepository.existsById(Mockito.anyInt())).willReturn(true);
        // Setup mock repository to return `updatedFeature` when `save()` is called
        given(featureRepository.save(Mockito.any(Feature.class))).willReturn(updatedFeature);

        mvc.perform(put("/api/v1/features/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updatedFeature)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.description", is("newDescription")))
                .andExpect(jsonPath("$.iconName", is("newIconName")));

        verify(featureRepository).existsById(3);
    }


    @Test
    public void cannotDeleteNonExistingFeature() throws Exception {
        // Setup mock repository to return `false` when `existsById()` is called
        given(featureRepository.existsById(Mockito.anyInt())).willReturn(false);

        mvc.perform(delete("/api/v1/features/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotDeleteWhenNotAuthenticated() throws Exception {
        mvc.perform(delete("/api/v1/features/1")).andExpect(status().isForbidden());
    }


    @Test
    public void canDeleteFeature() throws Exception {
        // Setup mock repository to return `true` when `existsById()` is called
        given(featureRepository.existsById(Mockito.anyInt())).willReturn(true);

        mvc.perform(delete("/api/v1/features/4"))
                .andExpect(status().isOk());

        verify(featureRepository).existsById(4);
        verify(featureRepository).deleteById(4);
    }


    @Test
    @WithAnonymousUser
    public void cannotListWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/features")).andExpect(status().isForbidden());
    }


    @Test
    public void canListFeatures() throws Exception {
        // Setup mock repository to return `testFeatures` when `findAll()` is called
        given(featureRepository.findAll()).willReturn(testFeatures);

        mvc.perform(get("/api/v1/features"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].description", is("feature1")))
                .andExpect(jsonPath("$[1].description", is("feature2")))
                .andExpect(jsonPath("$[2].description", is("feature3")));

        verify(featureRepository).findAll();
    }

}
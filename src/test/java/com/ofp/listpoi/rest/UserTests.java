package com.ofp.listpoi.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ofp.listpoi.controllers.UserController;
import com.ofp.listpoi.entities.User;
import com.ofp.listpoi.repositories.UserRepository;
import com.ofp.listpoi.security.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@Import(RestTestsConfiguration.class)
@WebMvcTest(UserController.class)
public class UserTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserService userService;

    // Default test instance
    private final User testUser = new User(true, true, true, "firstName", "lastName", "mail@test.com", "password");

    // Default list instance
    private final Collection<User> testUsers = Arrays.asList(
            new User(true, true, true, "firstName1", "lastName1", "mail@test.com", "password"),
            new User(true, true, true, "firstName2", "lastName2", "mail@test.com", "password"),
            new User(true, true, true, "firstName3", "lastName3", "mail@test.com", "password")
    );


    @Test
    @WithAnonymousUser
    public void cannotCreateWhenNotAuthenticated() throws Exception {
        mvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testUser)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canCreateUser() throws Exception {
        // Setup mock repository to return `testUser` when `save()` is called
        given(userService.signup(Mockito.any(User.class))).willReturn(testUser);

        mvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(testUser)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.firstName", is(testUser.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(testUser.getLastName())));
    }


    @Test
    public void cannotGetNonExistingUser() throws Exception {
        mvc.perform(get("/api/v1/users/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotGetWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/users/1")).andExpect(status().isForbidden());
    }


    @Test
    public void canGetUserById() throws Exception {
        // Setup mock repository to return `testUser` when `save()` is called
        given(userRepository.findById(Mockito.anyInt())).willReturn(Optional.of(testUser));

        mvc.perform(get("/api/v1/users/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.firstName", is(testUser.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(testUser.getLastName())));

        verify(userRepository).findById(2);
    }


    @Test
    public void cannotUpdateNonExistingUser() throws Exception {
        // Setup mock repository to return `false` when `existsById()` is called
        given(userRepository.existsById(Mockito.anyInt())).willReturn(false);

        mvc.perform(put("/api/v1/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(testUser)))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotUpdateWhenNotAuthenticated() throws Exception {
        mvc.perform(put("/api/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testUser)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canUpdateUser() throws Exception {
        User updatedUser = new User(true, true, true, "newFirstName", "newLastName", "mail@test.com", "password");

        // Setup mock repository to return `true` when `existsById()` is called
        given(userRepository.existsById(Mockito.anyInt())).willReturn(true);
        // Setup mock repository to return `updatedUser` when `save()` is called
        given(userService.update(Mockito.any(User.class))).willReturn(updatedUser);

        mvc.perform(put("/api/v1/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updatedUser)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.firstName", is("newFirstName")))
                .andExpect(jsonPath("$.lastName", is("newLastName")));

        verify(userRepository).existsById(3);
    }


    @Test
    public void cannotDeleteNonExistingUser() throws Exception {
        // Setup mock repository to return `false` when `existsById()` is called
        given(userRepository.existsById(Mockito.anyInt())).willReturn(false);

        mvc.perform(delete("/api/v1/users/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithAnonymousUser
    public void cannotDeleteWhenNotAuthenticated() throws Exception {
        mvc.perform(delete("/api/v1/users/1")).andExpect(status().isForbidden());
    }


    @Test
    public void canDeleteUser() throws Exception {
        // Setup mock repository to return `true` when `existsById()` is called
        given(userRepository.existsById(Mockito.anyInt())).willReturn(true);

        mvc.perform(delete("/api/v1/users/4"))
                .andExpect(status().isOk());

        verify(userRepository).existsById(4);
        verify(userRepository).deleteById(4);
    }


    @Test
    @WithAnonymousUser
    public void cannotListWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/users")).andExpect(status().isForbidden());
    }


    @Test
    public void canListUsers() throws Exception {
        // Setup mock repository to return `testUsers` when `findAll()` is called
        given(userRepository.findAll()).willReturn(testUsers);

        mvc.perform(get("/api/v1/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].firstName", is("firstName1")))
                .andExpect(jsonPath("$[1].firstName", is("firstName2")))
                .andExpect(jsonPath("$[2].firstName", is("firstName3")));

        verify(userRepository).findAll();
    }


    @Test
    public void canFilterUsers() throws Exception {
        // Setup mock repository to return `testUsers` when `findByNameContaining()` is called
        given(userRepository.filterBy(Mockito.anyString())).willReturn(testUsers);

        mvc.perform(get("/api/v1/users")
                .param("filter", "myFilter"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].firstName", is("firstName1")))
                .andExpect(jsonPath("$[1].firstName", is("firstName2")))
                .andExpect(jsonPath("$[2].firstName", is("firstName3")));

        verify(userRepository).filterBy("myFilter");
    }

}
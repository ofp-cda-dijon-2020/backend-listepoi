/* Sample trigger to update the POI rating - UNUSED (managed by the ORM) */

DELIMITER $$
CREATE TRIGGER updatePOIRating
    AFTER INSERT ON rating
    FOR EACH ROW
BEGIN
    DECLARE mean double;
    SET mean=(SELECT SUM(rating.rating) / COUNT(*) FROM rating WHERE rating.poi_id=NEW.poi_id);
    UPDATE poi SET rating=mean WHERE id=NEW.poi_id;
END$$
DELIMITER ;
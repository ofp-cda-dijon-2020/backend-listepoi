package com.ofp.listpoi;

public class GpsHelpers {

    public static final int earthRadiusKm = 6371;

    public static double computedDistance(double lat1, double lon1, double lat2, double lon2){

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return earthRadiusKm * c;
    }


    public static BoundingBox computeBoundingBox(double lat, double lon, double distance) {
        double dLat = distance * 180 / (Math.PI * earthRadiusKm);
        double dLon = distance * 180 / (Math.PI * earthRadiusKm * Math.cos(lat));
        return new BoundingBox(lat - dLat, lat + dLat, lon - dLon, lon + dLon);
    }


    public static class BoundingBox {
        private final double latMin, latMax, lonMin, lonMax;

        public BoundingBox(double latMin, double latMax, double lonMin, double lonMax) {
            if(latMin < latMax) {
                this.latMin = latMin;
                this.latMax = latMax;
            } else {
                this.latMin = latMax;
                this.latMax = latMin;
            }

            if(lonMin < lonMax) {
                this.lonMin = lonMin;
                this.lonMax = lonMax;
            } else {
                this.lonMax = lonMin;
                this.lonMin = lonMax;
            }
        }

        public double getLatMin() {
            return latMin;
        }

        public double getLatMax() {
            return latMax;
        }

        public double getLonMin() {
            return lonMin;
        }

        public double getLonMax() {
            return lonMax;
        }
    }
}
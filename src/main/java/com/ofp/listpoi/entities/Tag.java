package com.ofp.listpoi.entities;

import javax.persistence.*;

@Entity
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    /**
     * Default empty constructor for JPA / Jackson
     */
    @SuppressWarnings("unused")
    public Tag() {}

    /**
     * Default basic constructor
     *
     * @param name The name of the tag
     */
    public Tag(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

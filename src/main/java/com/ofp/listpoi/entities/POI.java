package com.ofp.listpoi.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class POI {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private double latitude;

    @Column(nullable = false)
    private double longitude;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column
    private float rating;

    @Column(columnDefinition = "BLOB")
    private byte[] photo;

    @Column(nullable = false)
    private Date creationDate;

    @ManyToOne(optional = false)
    private User creator;

    @ManyToOne(optional = false)
    private Category category;

    @ManyToMany
    private Collection<Tag> tags;

    @ManyToMany
    private Collection<Feature> features;

    @OneToMany(mappedBy = "poi")
    private Collection<Rating> ratings;


    /**
     * Default empty constructor for JPA / Jackson
     */
    @SuppressWarnings("unused")
    public POI() {}


    /**
     * Default basic constructor
     */
    public POI(double latitude, double longitude, String name, String description, float rating, byte[] photo, Date creationDate, User creator, Category category) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.photo = photo;
        this.creationDate = creationDate;
        this.creator = creator;
        this.category = category;
    }


    /**
     * Constructor with tags and features
     */
    public POI(double latitude, double longitude, String name, String description, float rating, byte[] photo, Date creationDate, User creator, Category category, Collection<Tag> tags, Collection<Feature> features) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.photo = photo;
        this.creationDate = creationDate;
        this.creator = creator;
        this.category = category;
        this.tags = tags;
        this.features = features;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Collection<Tag> getTags() {
        return tags;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

    public Collection<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(Collection<Feature> features) {
        this.features = features;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Collection<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Collection<Rating> ratings) {
        this.ratings = ratings;
    }

}

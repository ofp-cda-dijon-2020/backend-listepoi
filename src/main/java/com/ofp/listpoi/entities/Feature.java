package com.ofp.listpoi.entities;

import javax.persistence.*;

@Entity
public class Feature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String iconName;

    @Column
    private String description;

    /**
     * Default empty constructor for JPA / Jackson
     */
    @SuppressWarnings("unused")
    public Feature() {}


    /**
     * Default basic constructor
     *
     * @param iconName The icon's name for this feature
     * @param description The feature's description
     */
    public Feature(String iconName, String description) {
        this.iconName = iconName;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

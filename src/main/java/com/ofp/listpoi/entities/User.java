package com.ofp.listpoi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private boolean moderator;

    @Column
    private boolean active;

    @Column
    private boolean verified;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "creator")
    private Collection<POI> pois;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Collection<Rating> ratings;

    @JsonIgnore
    @ManyToMany
    private Collection<User> friends;

    /**
     * Default empty constructor for JPA / Jackson
     */
    @SuppressWarnings("unused")
    public User() {}




    /**
     * Default basic constructor
     * @param moderator Boolean flag indicating whether the user has moderator role
     * @param active Boolean flag indicating whether the user's account is active
     * @param verified Boolean flag indicating whether the user's account was verified
     * @param firstName The user's first name
     * @param lastName The user's last name
     * @param email The user's email
     * @param password
     */
    public User(boolean moderator, boolean active, boolean verified, String firstName, String lastName, String email, String password) {
        this.moderator = moderator;
        this.active = active;
        this.verified = verified;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isModerator() {
        return moderator;
    }

    public void setModerator(boolean moderator) {
        this.moderator = moderator;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore


    public Collection<POI> getPois() {
        return pois;
    }

    public void setPois(Collection<POI> pois) {
        this.pois = pois;
    }

    public Collection<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Collection<Rating> ratings) {
        this.ratings = ratings;
    }

    public Collection<User> getFriends() {
        return friends;
    }

    public void setFriends(Collection<User> friends) {
        this.friends = friends;
    }
}
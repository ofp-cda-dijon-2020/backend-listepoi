package com.ofp.listpoi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;

@Entity
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int rating;

    @Column
    private String comment;

    @ManyToOne(optional = false)
    private POI poi;

    @ManyToOne(optional = false)
    private User user;


    /**
     * Default empty constructor for JPA / Jackson
     */
    @SuppressWarnings("unused")
    private Rating() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonIgnore
    public POI getPoi() {
        return poi;
    }

    @JsonSetter
    public void setPoi(POI poi) {
        this.poi = poi;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    @JsonSetter
    public void setUser(User user) {
        this.user = user;
    }

}

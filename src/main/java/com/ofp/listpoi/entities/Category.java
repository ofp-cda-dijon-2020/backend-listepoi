package com.ofp.listpoi.entities;

import javax.persistence.*;

@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private String iconName;

    /**
     * Default empty constructor for JPA / Jackson
     */
    @SuppressWarnings("unused")
    public Category() {}


    /**
     * Basic constructor
     *
     * @param name The name of the category
     * @param iconName The icon's name for the category
     */
    public Category(String name, String iconName) {
        this.name = name;
        this.iconName = iconName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

}
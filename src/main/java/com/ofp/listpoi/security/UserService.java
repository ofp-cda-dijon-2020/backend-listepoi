package com.ofp.listpoi.security;

import com.ofp.listpoi.entities.User;
import com.ofp.listpoi.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;


    /**
     *
     * @param username
     * @param password
     * @return
     */
    public String login(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        return jwtTokenProvider.createToken(username);
    }


    /**
     *
     * @param remoteUser
     * @return
     */
    public User whoami(String remoteUser) {
        return userRepository.findByEmail(remoteUser).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No such user"));
    }


    /**
     *
     * @param remoteUser
     * @return
     */
    public String refreshToken(String remoteUser) {
        return jwtTokenProvider.createToken(remoteUser);
    }


    /**
     *
     * @param user
     * @return
     */
    public User signup(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }


    /**
     *
     * @param user
     * @return
     */
    public User update(User user) {
        if(user.getPassword() != null)
            user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

}

package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.Rating;

public interface CustomRatingRepository {
    Rating save(Rating rating);
}

package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.POI;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface POIRepository extends CrudRepository<POI, Integer>, CustomPOIRepository {
    Iterable<POI> findAllByCategoryId(int categoryId);
    Iterable<POI> findAllByCreatorId(int creatorId);

    @Query("select p from POI p join p.features f where f.id = ?1")
    Iterable<POI> findAllByFeatureId(int categoryId);

    @Query("select p from POI p join p.tags t where t.id = ?1")
    Iterable<POI> findAllByTagId(int tagId);

    @Query("select p from POI p where p.name like %?1% or p.description like %?1%")
    Iterable<POI> filterBy(String filter);

    @Query("select p from POI p where p.latitude >= ?1 and p.latitude <= ?2 and p.longitude >= ?3 and p.longitude <= ?4")
    Iterable<POI> filterByDistanceSquare(double latMin, double latMax, double lonMin, double lonMax);
}

package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.AccountValidation;
import org.springframework.data.repository.CrudRepository;

public interface AccountValidationRepository extends CrudRepository<AccountValidation, Integer> {
}

package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
    Iterable<Category> findAllByNameContaining(String name);
}

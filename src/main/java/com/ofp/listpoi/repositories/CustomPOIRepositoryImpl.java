package com.ofp.listpoi.repositories;

import com.ofp.listpoi.GpsHelpers;
import com.ofp.listpoi.entities.POI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.LinkedList;

@Repository
@SuppressWarnings("unused")
public class CustomPOIRepositoryImpl implements CustomPOIRepository {

    private POIRepository poiRepository;

    @Autowired
    public void setPOIRepository(@Lazy POIRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    @Override
    public Iterable<POI> filterByDistance(double latitude, double longitude, double distance) {
        GpsHelpers.BoundingBox box = GpsHelpers.computeBoundingBox(latitude, longitude, distance);
        Iterable<POI> all = poiRepository.filterByDistanceSquare(box.getLatMin(), box.getLatMax(), box.getLonMin(), box.getLonMax());

        Collection<POI> result = new LinkedList<>();

        for(POI p : all) {
            if(GpsHelpers.computedDistance(latitude, longitude, p.getLatitude(), p.getLongitude()) <= distance)
                result.add(p);
        }

        return result;
    }

}

package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {
    @Query("select p from User p where p.firstName like %?1% or p.lastName like %?1%")
    Iterable<User> filterBy(String filter);

    Optional<User> findByEmail(String email);
}

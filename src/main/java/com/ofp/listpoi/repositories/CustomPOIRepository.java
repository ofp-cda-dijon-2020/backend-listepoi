package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.POI;

public interface CustomPOIRepository {
    Iterable<POI> filterByDistance(double latitude, double longitude, double distance);
}

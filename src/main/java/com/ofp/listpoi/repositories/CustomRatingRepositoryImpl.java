package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.POI;
import com.ofp.listpoi.entities.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@SuppressWarnings("unused")
public class CustomRatingRepositoryImpl implements CustomRatingRepository {

    private RatingRepository ratingRepository;
    private POIRepository poiRepository;

    @Autowired
    public void setRatingRepository(@Lazy RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Autowired
    public void setPOIRepository(@Lazy POIRepository poiRepository) {
        this.poiRepository = poiRepository;
    }


    /**
     * Override the default save to compute the poi's average rating on create/update
     *
     * @param rating A rating instance to save
     * @return The saved rating
     */
    @Override
    public Rating save(Rating rating) {
        Rating result = ((CrudRepository<Rating, Integer>) this.ratingRepository).save(rating);
        Collection<Rating> ratings = this.ratingRepository.findByPoiId(rating.getPoi().getId());

        int size = 0;
        float mean = 0;
        for(Rating r : ratings) {
            size++;
            mean += r.getRating();
        }
        mean = mean / size;

        POI poi = poiRepository.findById(rating.getPoi().getId()).orElseThrow();
        poi.setRating(mean);
        poiRepository.save(poi);

        return result;
    }

}

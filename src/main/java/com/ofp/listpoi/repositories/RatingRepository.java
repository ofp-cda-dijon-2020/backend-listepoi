package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.Rating;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface RatingRepository extends CrudRepository<Rating, Integer>, CustomRatingRepository {
    Collection<Rating> findByPoiId(int id);
}

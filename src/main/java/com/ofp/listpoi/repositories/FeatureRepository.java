package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.Feature;
import org.springframework.data.repository.CrudRepository;

public interface FeatureRepository extends CrudRepository<Feature, Integer> {
}

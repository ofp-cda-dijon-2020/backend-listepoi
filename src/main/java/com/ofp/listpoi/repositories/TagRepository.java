package com.ofp.listpoi.repositories;

import com.ofp.listpoi.entities.Tag;
import org.springframework.data.repository.CrudRepository;

public interface TagRepository extends CrudRepository<Tag, Integer> {
    Iterable<Tag> findAllByNameContaining(String filter);
}

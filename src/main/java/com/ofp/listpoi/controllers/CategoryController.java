package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.Category;
import com.ofp.listpoi.repositories.CategoryRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class CategoryController {

    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/api/v1/categories/{categoryId}")
    public Category get(@PathVariable int categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/categories")
    public Iterable<Category> list(@RequestParam(name = "filter", required = false) String filter) {
        if(filter != null)
            return categoryRepository.findAllByNameContaining(filter);
        else
            return categoryRepository.findAll();
    }

    @PostMapping(path = "/api/v1/categories", headers = "Accept=application/json")
    public Category create(@RequestBody Category category) {
        return categoryRepository.save(category);
    }

    @DeleteMapping("/api/v1/categories/{categoryId}")
    public void delete(@PathVariable int categoryId) {
        if(categoryRepository.existsById(categoryId))
            categoryRepository.deleteById(categoryId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/categories/{categoryId}")
    public Category update(@PathVariable int categoryId, @RequestBody Category category) {
        if(!categoryRepository.existsById(categoryId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        category.setId(categoryId);
        return categoryRepository.save(category);
    }

}

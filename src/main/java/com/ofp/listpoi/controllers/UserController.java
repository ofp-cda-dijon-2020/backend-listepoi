package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.User;
import com.ofp.listpoi.repositories.UserRepository;
import com.ofp.listpoi.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;


    @GetMapping("/api/v1/users/{userId}")
    public User get(@PathVariable int userId) {
        return userRepository.findById(userId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/users")
    public Iterable<User> list(@RequestParam(name = "filter", required = false) String filter) {
        if(filter != null)
            return userRepository.filterBy(filter);
        else
            return userRepository.findAll();
    }

    @PostMapping(path = "/api/v1/users", headers = "Accept=application/json")
    public User create(@RequestBody User user) {
        return userService.signup(user);
    }

    @DeleteMapping("/api/v1/users/{userId}")
    public void delete(@PathVariable int userId) {
        if(userRepository.existsById(userId))
            userRepository.deleteById(userId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/users/{userId}")
    public User update(@PathVariable int userId, @RequestBody User user) {
        if(!userRepository.existsById(userId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        user.setId(userId);
        return userService.update(user);
    }

}

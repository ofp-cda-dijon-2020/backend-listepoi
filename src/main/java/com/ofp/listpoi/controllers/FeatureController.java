package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.Feature;
import com.ofp.listpoi.repositories.FeatureRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class FeatureController {

    private final FeatureRepository featureRepository;

    public FeatureController(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }

    @GetMapping("/api/v1/features/{featureId}")
    public Feature get(@PathVariable int featureId) {
        return featureRepository.findById(featureId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/features")
    public Iterable<Feature> list() {
        return featureRepository.findAll();
    }

    @PostMapping(path = "/api/v1/features", headers = "Accept=application/json")
    public Feature create(@RequestBody Feature feature) {
        return featureRepository.save(feature);
    }

    @DeleteMapping("/api/v1/features/{featureId}")
    public void delete(@PathVariable int featureId) {
        if(featureRepository.existsById(featureId))
            featureRepository.deleteById(featureId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/features/{featureId}")
    public Feature update(@PathVariable int featureId, @RequestBody Feature feature) {
        if(!featureRepository.existsById(featureId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        feature.setId(featureId);
        return featureRepository.save(feature);
    }

}

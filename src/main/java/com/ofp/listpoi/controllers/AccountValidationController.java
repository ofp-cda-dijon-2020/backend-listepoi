package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.AccountValidation;
import com.ofp.listpoi.repositories.AccountValidationRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class AccountValidationController {

    private final AccountValidationRepository accountValidationRepository;

    public AccountValidationController(AccountValidationRepository accountValidationRepository) {
        this.accountValidationRepository = accountValidationRepository;
    }

    @GetMapping("/api/v1/accountValidations/{accountValidationId}")
    public AccountValidation get(@PathVariable int accountValidationId) {
        return accountValidationRepository.findById(accountValidationId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/accountValidations")
    public Iterable<AccountValidation> list() {
        return accountValidationRepository.findAll();
    }

    @PostMapping(path = "/api/v1/accountValidations", headers = "Accept=application/json")
    public AccountValidation create(@RequestBody AccountValidation accountValidation) {
        return accountValidationRepository.save(accountValidation);
    }

    @DeleteMapping("/api/v1/accountValidations/{accountValidationId}")
    public void delete(@PathVariable int accountValidationId) {
        if(accountValidationRepository.existsById(accountValidationId))
            accountValidationRepository.deleteById(accountValidationId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/accountValidations/{accountValidationId}")
    public AccountValidation update(@PathVariable int accountValidationId, @RequestBody AccountValidation accountValidation) {
        if(!accountValidationRepository.existsById(accountValidationId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        accountValidation.setId(accountValidationId);
        return accountValidationRepository.save(accountValidation);
    }

}

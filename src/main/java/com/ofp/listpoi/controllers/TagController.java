package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.Tag;
import com.ofp.listpoi.repositories.TagRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class TagController {

    private final TagRepository tagRepository;

    public TagController(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @GetMapping("/api/v1/tags/{tagId}")
    public Tag get(@PathVariable int tagId) {
        return tagRepository.findById(tagId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/tags")
    public Iterable<Tag> list(@RequestParam(name = "filter", required = false) String filter) {
        if(filter != null)
            return tagRepository.findAllByNameContaining(filter);
        else
            return tagRepository.findAll();
    }

    @PostMapping(path = "/api/v1/tags", headers = "Accept=application/json")
    public Tag create(@RequestBody Tag tag) {
        return tagRepository.save(tag);
    }

    @DeleteMapping("/api/v1/tags/{tagId}")
    public void delete(@PathVariable int tagId) {
        if(tagRepository.existsById(tagId))
            tagRepository.deleteById(tagId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/tags/{tagId}")
    public Tag update(@PathVariable int tagId, @RequestBody Tag tag) {
        if(!tagRepository.existsById(tagId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        tag.setId(tagId);
        return tagRepository.save(tag);
    }

}

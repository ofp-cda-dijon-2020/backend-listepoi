package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.POI;
import com.ofp.listpoi.repositories.POIRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.Date;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class POIController {

    private final POIRepository poiRepository;

    public POIController(POIRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    @GetMapping("/api/v1/pois/{poiId}")
    public POI get(@PathVariable int poiId) {
        return poiRepository.findById(poiId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/pois")
    public Iterable<POI> list() {
        return poiRepository.findAll();
    }

    @PostMapping(path = "/api/v1/pois", headers = "Accept=application/json")
    public POI create(@RequestBody POI poi) {
        poi.setCreationDate(Date.from(Instant.now()));
        return poiRepository.save(poi);
    }

    @DeleteMapping("/api/v1/pois/{poiId}")
    public void delete(@PathVariable int poiId) {
        if(poiRepository.existsById(poiId))
            poiRepository.deleteById(poiId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/pois/{poiId}")
    public POI update(@PathVariable int poiId, @RequestBody POI poi) {
        if(!poiRepository.existsById(poiId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        poi.setId(poiId);
        return poiRepository.save(poi);
    }

}

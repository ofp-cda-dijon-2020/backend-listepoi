package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.User;
import com.ofp.listpoi.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthenticationController {

    @Autowired
    private UserService userService;


    @PostMapping("/api/v1/auth/login")
    public String login(@RequestParam String username, @RequestParam String password) {
        return userService.login(username, password);
    }


    @GetMapping("/api/v1/auth/refresh")
    public String refresh(HttpServletRequest req) {
        return userService.refreshToken(req.getRemoteUser());
    }


    @GetMapping("/api/v1/auth/whoami")
    public User whoami(HttpServletRequest req) {
        return userService.whoami(req.getRemoteUser());
    }


    @PostMapping(path = "/api/v1/auth/signup", headers = "Accept=application/json")
    public User signup(@RequestBody User user) {
        return userService.signup(user);
    }
}

package com.ofp.listpoi.controllers;

import com.ofp.listpoi.entities.Rating;
import com.ofp.listpoi.repositories.CustomRatingRepository;
import com.ofp.listpoi.repositories.RatingRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class RatingController {

    private final RatingRepository ratingRepository;

    public RatingController(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @GetMapping("/api/v1/ratings/{ratingId}")
    public Rating get(@PathVariable int ratingId) {
        return ratingRepository.findById(ratingId).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find resource"));
    }

    @GetMapping("/api/v1/ratings")
    public Iterable<Rating> list() {
       return ratingRepository.findAll();
    }

    @PostMapping(path = "/api/v1/ratings", headers = "Accept=application/json")
    public Rating create(@RequestBody Rating rating) {
        return ((CustomRatingRepository) ratingRepository).save(rating);
    }

    @DeleteMapping("/api/v1/ratings/{ratingId}")
    public void delete(@PathVariable int ratingId) {
        if(ratingRepository.existsById(ratingId))
            ratingRepository.deleteById(ratingId);
        else
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
    }

    @PutMapping("/api/v1/ratings/{ratingId}")
    public Rating update(@PathVariable int ratingId, @RequestBody Rating rating) {
        if(!ratingRepository.existsById(ratingId))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        rating.setId(ratingId);
        return ((CustomRatingRepository) ratingRepository).save(rating);
    }

}

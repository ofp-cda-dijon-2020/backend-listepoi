package com.ofp.listpoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListPOIApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListPOIApplication.class, args);
	}

}
